<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{

		// $all_pekan  = [  
		// 	[
		// 		'nama' => 'Seremban',
		// 		'gambar' => 'https://images.unsplash.com/photo-1615870795127-1c13f1d92997?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Z2VtYXMlMjBuZWdlcmklMjBzZW1iaWxhbnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit quo fuga delectus, esse enim explicabo reiciendis atque illo quas cum blanditiis aperiam magnam modi! Ipsam beatae vitae facere quaerat amet!'
		// 	],

		// 	[
		// 		'nama' => 'Kuala pilah',
		// 		'gambar' => 'https://images.unsplash.com/photo-1585665936960-e7c7906262ef?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Z2VtYXMlMjBuZWdlcmklMjBzZW1iaWxhbnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit quo fuga delectus, esse enim explicabo reiciendis atque illo quas cum blanditiis aperiam magnam modi! Ipsam beatae vitae facere quaerat amet!'
		// 	],

		// 	[
		// 		'nama' => 'Gemas',
		// 		'gambar' => 'https://images.unsplash.com/photo-1588358580561-b82b59d4dc7e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGdlbWFzJTIwbmVnZXJpJTIwc2VtYmlsYW58ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit quo fuga delectus, esse enim explicabo reiciendis atque illo quas cum blanditiis aperiam magnam modi! Ipsam beatae vitae facere quaerat amet!'
		// 	],
 
		// 	[
		// 		'nama' => 'juaseh',
		// 		'gambar' => 'https://images.unsplash.com/photo-1618191218087-a515ea09bda5?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjN8fGdlbWFzJTIwbmVnZXJpJTIwc2VtYmlsYW58ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit quo fuga delectus, esse enim explicabo reiciendis atque illo quas cum blanditiis aperiam magnam modi! Ipsam beatae vitae facere quaerat amet!'
		// 	],

		// 	[
		// 		'nama' => 'Rembau',
		// 		'gambar' => 'https://images.unsplash.com/photo-1597805539244-3f0848b99ad7?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjR8fGdlbWFzJTIwbmVnZXJpJTIwc2VtYmlsYW58ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit quo fuga delectus, esse enim explicabo reiciendis atque illo quas cum blanditiis aperiam magnam modi! Ipsam beatae vitae facere quaerat amet!'
		// 	],

		// 	[
		// 		'nama' => 'Port Dickson',
		// 		'gambar' => 'https://images.unsplash.com/photo-1505716619202-1193d03557c4?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjB8fGdlbWFzJTIwbmVnZXJpJTIwc2VtYmlsYW58ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit quo fuga delectus, esse enim explicabo reiciendis atque illo quas cum blanditiis aperiam magnam modi! Ipsam beatae vitae facere quaerat amet!'
		// 	],

		// ];

		$db = db_connect();
		$result = $db->query('select * from gambar');
		$all_pekan = $result->getResult();

		// dd( $all_pekan );

		return view('homepage' , ['all_pekan'=> $all_pekan]);
	}
}
